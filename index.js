var input = document.querySelector('#field');
var btn = document.querySelector("#btn");
var list = document.querySelector("#list");
var toDoList = [];
var diplayMessages = function () {
    var dispalyMessage = "";
    if (toDoList.length === 0)
        list.innerHTML = "Список Задач пуст";
    toDoList.forEach(function (item, index) {
        dispalyMessage += "\n        <li>\n            <label>\n                <input type=\"checkbox\" " + (item.checked ? "checked" : "") + " />\n                <span id=\"check_" + index + "\" >" + item.todo + "</span>\n                <button class=\"btn\" id=\"item_" + index + "\">Delete</button>\n            </label>\n        </li>";
        list.innerHTML = dispalyMessage;
    });
};
if (localStorage.getItem("todo")) {
    toDoList = JSON.parse(localStorage.getItem("todo"));
    diplayMessages();
}
btn.addEventListener("click", function () {
    if (input.value !== "") {
        var newTodo = {
            todo: input.value,
            checked: false
        };
        toDoList.push(newTodo);
        diplayMessages();
        localStorage.setItem("todo", JSON.stringify(toDoList));
    }
    input.value = "";
});
list.addEventListener('click', function (e) {
    toDoList.forEach(function (item, index) {
        if ("item_" + index === e.target.id) {
            toDoList.splice(index, 1);
        }
        else if ("check_" + index === e.target.id) {
            item.checked = !item.checked;
        }
        localStorage.setItem("todo", JSON.stringify(toDoList));
        diplayMessages();
    });
});
